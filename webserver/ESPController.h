#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <Arduino.h>
#include <ESP8266WiFi.h>

enum Mode { MANAGED, AUTOMATIC };

class ESPController {
public:

  void setTemperatureOpen(int temp);
  void setTemperatureClose(int temp);
  void setHumidityOpen(int humidity);
  void setHumidityClose(int humidity);

  void setMode(int mode);
  void setPeriod(int period);
  
  void open();
  void close();
  
  float getAirTemperature();
  float getAirHumidity();
  float getSoilHumidity();

  void getSettings();
  int getDoorState();

private:
};

#endif
