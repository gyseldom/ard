#include <ESP8266WiFi.h>        // Include the Wi-Fi library
#include <ESP8266WebServer.h>
#include "ESPController.h"

#define ESP_BAUD_RATE 115200

const char *ssid = "sklenik"; 
const char *password = "123456789";

IPAddress subnet(255, 255, 0, 0);	// Subnet Mask
IPAddress gateway(192, 168, 1, 1); // Default Gateway
IPAddress local_IP(192, 168, 1, 10); // Static IP Address for ESP8266

ESP8266WebServer server(80);
ESPController controller;

void handleSetTemperatureClose() {
  if (server.args() != 1) {
    server.send(422, "text/plain", "Invalid argument");
    return;
  }

  controller.setTemperatureClose(server.arg(0).toInt());

  server.sendHeader("Location", "/", true);
  server.send(302, "text/plain", "");
}
void handleSetTemperatureOpen () {
  if (server.args() != 1) {
    server.send(422, "text/plain", "Invalid argument");
    return;
  }

  controller.setTemperatureOpen(server.arg(0).toInt());

  server.sendHeader("Location", "/", true);
  server.send(302, "text/plain", "");
}
void handleSetHumidityClose () {
  if (server.args() != 1) {
    server.send(422, "text/plain", "Invalid argument");
    return;
  }

  controller.setHumidityClose(server.arg(0).toInt());

  server.sendHeader("Location", "/", true);
  server.send(302, "text/plain", "");
}
void handleSetHumidityOpen () {
  if (server.args() != 1) {
    server.send(422, "text/plain", "Invalid argument");
    return;
  }

  controller.setHumidityOpen(server.arg(0).toInt());

  server.sendHeader("Location", "/", true);
  server.send(302, "text/plain", "");
}
void handleSetMode () {
  if (server.args() != 1) {
    server.send(422, "text/plain", "Invalid argument");
    return;
  }

  controller.setMode(server.arg(0).toInt());

  server.sendHeader("Location", "/", true);
  server.send(302, "text/plain", "");
}
void handleSetPeriod () {
  if (server.args() != 1) {
    server.send(422, "text/plain", "Invalid argument");
    return;
  }

  controller.setPeriod(server.arg(0).toInt());

  server.sendHeader("Location", "/", true);
  server.send(302, "text/plain", "");
}

void handleOpen() {
  controller.open();
  server.sendHeader("Location", "/", true);
  server.send(302, "text/plain", "");
}
void handleClose() {
  controller.close();
  server.sendHeader("Location", "/", true);
  server.send(302, "text/plain", "");
}

void handleGetSettings () {
  Serial.print("getSettings");

  int temperatureClose = 0;
  int temperatureOpen = 0;
  int humidityClose = 0;
  int humidityOpen = 0;

  int mode = 0;
  int period = 0;

  // wait for response from arduino
  for (int i = 0; i < 1000; i++) {
    if(Serial.available()) {
      break;
    }
    delay(1);
  }

  for (int i = 0; i < 6; i++) {
    if (!Serial.available()) {
      i--;
      continue;
    }
    String line = Serial.readStringUntil('\n');
    Serial.read();

    if (line.startsWith("humidityOpen")) {
      humidityOpen = line.substring(line.indexOf(" ")+1).toInt();
    } else if (line.startsWith("humidityClose")) {
      humidityClose = line.substring(line.indexOf(" ")+1).toInt();
    } else if (line.startsWith("temperatureOpen")) {
      temperatureOpen = line.substring(line.indexOf(" ")+1).toInt();
    } else if (line.startsWith("temperatureClose")) {
      temperatureClose = line.substring(line.indexOf(" ")+1).toInt();
    } else if (line.startsWith("mode")) {
      mode = line.substring(line.indexOf(" ")+1).toInt();
    } else if (line.startsWith("period")) {
      period = line.substring(line.indexOf(" ")+1).toInt();
    }
  }

  String html ="<!DOCTYPE html> <html lang=\"en\"> <head> <meta charset=\"UTF-8\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> <title>Settings Information</title> <style> body { font-family: Arial, sans-serif; margin: 0; padding: 20px; } h1 { color: #333; } h2 { margin-top: 20px; } table { border-collapse: collapse; width: 100%; } th, td { border: 1px solid #ddd; padding: 8px; text-align: left; } th { background-color: #f2f2f2; } </style> </head> <body> <h1>Settings Information</h1> <table> <tr> <td>Temperature Close threshold</td> <td>";
  html += temperatureClose;
  html +="</td> </tr> <tr> <td>Temperature Open threshold</td> <td>";
  html += temperatureOpen;
  html +="</td> </tr> <tr> <td>Humidity Close threshold</td> <td>";
  html += humidityClose;
  html +="</td> </tr> <tr> <td>Humidity Open threshold</td> <td>";
  html += humidityOpen;
  html +="</td> </tr> <tr> <td>Current Mode</td> <td>";
  if (mode) {
    html += "Automatic";
  } else {
    html += "Managed";
  }
  html +="</td> </tr> <tr> <td>Period</td> <td>";
  html += period;
  // html +="</td> </tr> <tr> <td>Door state</td> <td>";
  // html += getDoorState();
  html +="</td> </tr> </table> </body> </html>";

  
  server.send(200, "text/html", html);
}


void handleRootPage() {

String html ="<!DOCTYPE html> <html> <head> <title>Spravce skleniku</title> <style> body { font-family: Arial, sans-serif; margin: 0; padding: 20px; background-color: #f5f5f5; } h1 { color: #333; } a { color: #007bff; text-decoration: none; } a:hover { text-decoration: underline; } form { margin-bottom: 10px; } label { display: block; margin-bottom: 5px; } input[type=\"text\"] { width: 200px; padding: 5px; } input[type=\"submit\"] { padding: 5px 10px; background-color: #007bff; color: #fff; border: none; cursor: pointer; } input[type=\"submit\"]:hover { background-color: #0056b3; } </style> </head> <body> <h1>Spravce skleniku</h1> <a href=\"open\">Open</a><br><a href=\"close\">Close</a><br> <a href=\"getSettings\">Get Settings</a><br> <form action=\"/setTemperatureClose\"> <label for=\"setTemperatureClose\">Set Temperature Close</label> <input type=\"text\" name=\"temperature\" value=\"\"> <input type=\"submit\" value=\"Submit\"> </form> <form action=\"/setTemperatureOpen\"> <label for=\"setTemperatureOpen\">Set Temperature Open</label> <input type=\"text\" name=\"temperature\" value=\"\"> <input type=\"submit\" value=\"Submit\"> </form> <form action=\"/setHumidityClose\"> <label for=\"setHumidityClose\">Set Humidity Close</label> <input type=\"text\" name=\"humidity\" value=\"\"> <input type=\"submit\" value=\"Submit\"> </form> <form action=\"/setHumidityOpen\"> <label for=\"setHumidityOpen\">Set Humidity Open</label> <input type=\"text\" name=\"humidity\" value=\"\"> <input type=\"submit\" value=\"Submit\"> </form> <form action=\"/setMode\" style=\"display: flex; align-items: center;\"> <input type=\"radio\" id=\"managed\" name=\"mode\" value=\"0\" checked style=\"vertical-align: middle;\"> <label for=\"managed\" style=\"margin-right: 10px;\">Managed</label> <input type=\"radio\" id=\"automatic\" name=\"mode\" value=\"1\" style=\"vertical-align: middle;\"> <label for=\"automatic\" style=\"margin-right: 10px;\">Automatic</label> <input type=\"submit\" value=\"Submit\"> </form> <form action=\"/setPeriod\"> <label for=\"setPeriod\">Set Period</label> <input type=\"text\" name=\"period\" value=\"\"> <input type=\"submit\" value=\"Submit\"> </form> <br> <h1>Information</h1> <table> <tr> <td>Air Temperature</td> <td>";

html += String(controller.getAirTemperature());
html +=" C</td> </tr> <tr> <td>Air Humidity</td> <td>";
html += String(controller.getAirHumidity());
html +=" %</td> </tr> <tr> <td>Soil Humidity</td> <td>";
html += String(controller.getSoilHumidity());
html +=" %</td> </tr> <tr> <td>Door state</td> <td>";
if (controller.getDoorState()) {
  html += "Open";
} else {
  html += "Closed";
}
html +="</td> </tr> </table> </body> </html>";

  server.send(200, "text/html", html);
}

void handleUnknownPage() {
  String zprava = "Unknown page";
  zprava += "URI: ";
  zprava += server.uri();

  server.send(404, "text/plain", zprava);
}

void setupWiFi() {
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(local_IP, gateway, subnet);
  WiFi.softAP(ssid, password);
}

void setupServer() {
  server.on("/", handleRootPage);
  server.on("/open", handleOpen);
  server.on("/close", handleClose);
  server.on("/setTemperatureClose", handleSetTemperatureClose);
  server.on("/setTemperatureOpen", handleSetTemperatureOpen);
  server.on("/setHumidityClose", handleSetHumidityClose);
  server.on("/setHumidityOpen", handleSetHumidityOpen);
  server.on("/setMode", handleSetMode);
  server.on("/setPeriod", handleSetPeriod);
  server.on("/getSettings", handleGetSettings);
  server.onNotFound(handleUnknownPage);

  server.begin();
  Serial.println("HTTP server is running");
}

void setup() {
  Serial.begin(115200);

  setupWiFi();
  
  setupServer();
}

// main loop of ESP8266
// only handles requests from webpage
// doesn't expect any messages from arduino without ESP initiating first
void loop() {
  server.handleClient();
}
