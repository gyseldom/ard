#include "ESPController.h"


void ESPController::setTemperatureOpen(int temp) {
  Serial.print(String("setTemperatureOpen ") + String(temp) + "\n");
}

void ESPController::setTemperatureClose(int temp) {
  Serial.print(String("setTemperatureClose ") + String(temp) + "\n");
}

void ESPController::setHumidityOpen(int humidity) {
  Serial.print(String("setHumidityOpen ") + String(humidity) + "\n");
}

void ESPController::setHumidityClose(int humidity) {
  Serial.print(String("setHumidityClose ") + String(humidity) + "\n");
}

void ESPController::setMode(int mode) {
  Serial.print(String("setMode ") + String(mode) + "\n");
}

void ESPController::setPeriod(int period) {
  Serial.print(String("setPeriod ") + String(period) + "\n");
}

void ESPController::getSettings() { Serial.print("getSettings\n"); }

void ESPController::open() { Serial.print("open\n"); }

void ESPController::close() { Serial.print("close\n"); }

float ESPController::getAirTemperature() {
  Serial.print("getAirTemperature\n");

  while (!Serial.available()) {
    delay(5);
  }

  return Serial.readString().toFloat();
}

float ESPController::getAirHumidity() {
  Serial.print("getAirHumidity\n");

  while (!Serial.available()) {
    delay(5);
  }

  return Serial.readString().toFloat();
}

float ESPController::getSoilHumidity() {
  Serial.print("getSoilHumidity\n");

  while (!Serial.available()) {
    delay(5);
  }

  return Serial.readString().toFloat();
}

int ESPController::getDoorState() {
  Serial.print("getDoorState\n");

  while (!Serial.available()) {
    delay(5);
  }

  return Serial.readString().toInt();
}
