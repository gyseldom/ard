#include "ArduinoController.h"


// Setter for the open temperature threshold
void ArduinoController::setTemperatureOpen(int temp) {
  temperatureOpen = temp;
}

// Setter for the close temperature threshold
void ArduinoController::setTemperatureClose(int temp) {
  temperatureClose = temp;
}

// Setter for the open humidity threshold
void ArduinoController::setHumidityOpen(int humidity) {
  humidityOpen = humidity;
}

// Setter for the close humidity threshold
void ArduinoController::setHumidityClose(int humidity) {
  humidityClose = humidity;
}

// Setter for the mode
void ArduinoController::setMode(int mode) {
  this->mode = mode;
}

// Setter for the period
void ArduinoController::setPeriod(int period) {
  this->period = period;
}

// Send current settings to esp
void ArduinoController::getSettings() {
  String humiditySettings = "humidityOpen " + String(humidityOpen) + "\n" +
                            "humidityClose " + String(humidityClose) + "\n";
  String temperatureSettings = "temperatureOpen " + String(temperatureOpen) + "\n" +
                                "temperatureClose " + String(temperatureClose) + "\n";
  String modeSettings = "mode " + String(this->mode) + "\n";
  String periodSettings = "period " + String(this->period) + "\n";

  Serial.print(humiditySettings + temperatureSettings + modeSettings + periodSettings);
}

// Check conditions and open/close door accordingly
void ArduinoController::checkDoor() {
  auto temp = information.getAirTemperature();

  if (temp > temperatureOpen) {
    door.open();
    return;
  }

  if (temp < temperatureClose) {
    door.close();
    return;
  }

  auto humidity = information.getAirHumidity();

  if (humidity > humidityOpen) {
    door.open();
    return;
  }

  if (humidity < humidityClose) {
    door.close();
    return;
  }
}
