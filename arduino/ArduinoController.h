#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "Door.h"
#include "Information.h"
#include <Arduino.h>

class ArduinoController {
public:
  // Setters for temperature and humidity thresholds
  void setTemperatureOpen(int temp);
  void setTemperatureClose(int temp);
  void setHumidityOpen(int humidity);
  void setHumidityClose(int humidity);

  // Set the operating mode
  void setMode(int mode);

  // Set the period
  void setPeriod(int period);

  // Get current threshold settings
  void getSettings();

  // Check the door if it needs to be closed/opened
  void checkDoor();

  Door door = Door(8, 9, 10, 11);
  Information information;

private:
  // Default settings values
  int temperatureOpen = 30;
  int temperatureClose = 20;
  int humidityOpen = 70;
  int humidityClose = 50;
  int mode = 0;     // Default mode is MANAGED
  int period = 1;
};

#endif
