#include "Door.h"

// Constructor initializes motor pins and settings
Door::Door(int in1, int in2, int in3, int in4, int angle, int speed)
    : in1(in1), in2(in2), in3(in3), in4(in4), angle(angle), speed(speed) {
  // Set motor pins
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
}

bool Door::open() {
  if (isOpen) {
    return false;
  }

  // 512 cycles is 360 degrees
  for (long long i = 0; i < (angle * 64 / 45); i++) {
    rotateCW();
  }
  isOpen = true;
  return true;
}

bool Door::close() {
  if (!isOpen) {
    return false;
  }
  
  // 512 cycles is 360 degrees
  for (int i = 0; i < (angle * 64 / 45); i++) {
    rotateCCW();
  }
  isOpen = false;
  return true;
}

bool Door::getDoorState() {
  return isOpen;
}

void Door::rotateCW() {
  step1();
  step2();
  step3();
  step4();
  step5();
  step6();
  step7();
  step8();
}

void Door::rotateCCW() {
  step8();
  step7();
  step6();
  step5();
  step4();
  step3();
  step2();
  step1();
}

void Door::step1() {
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
  delay(speed);
}
void Door::step2() {
  digitalWrite(in1, HIGH);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
  delay(speed);
}
void Door::step3() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
  delay(speed);
}
void Door::step4() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  delay(speed);
}
void Door::step5() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  delay(speed);
}
void Door::step6() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, HIGH);
  delay(speed);
}
void Door::step7() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  delay(speed);
}
void Door::step8() {
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  delay(speed);
}
