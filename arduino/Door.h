#ifndef DOOR_H
#define DOOR_H

#include <Arduino.h>

class Door {
public:
  Door(int in1, int in2, int in3, int in4, int angle = 360, int speed = 1);

  // returns true if value changed otherwise false
  bool open();

  // returns true if value changed otherwise false
  bool close();

  bool getDoorState();

private:
  bool isOpen = false; // Current state of the door
  const int in1;       // Motor input pins
  const int in2;
  const int in3;
  const int in4;
  int speed;
  int angle;

  // Helper methods for motor control
  void rotateCW();
  void rotateCCW();
  void step1();
  void step2();
  void step3();
  void step4();
  void step5();
  void step6();
  void step7();
  void step8();
};

#endif
