#include "Information.h"
#include "ArduinoController.h"

#define ESP_BAUD_RATE 115200

// Enumeration for different modes
enum Mode { 
  MANAGED,    // Doors are operated manually
  AUTOMATIC   // Doors are operated automatically
};

Mode MODE_STATE = MANAGED;  // Initial mode is set to managed

ArduinoController controller;  // Instance of ArduinoController class

// Handle setting values
void handleSet(String message) {
  String sub = message.substring(message.indexOf(' '));
  if (message.startsWith("TemperatureClose")) {
    controller.setTemperatureClose(sub.toInt());
  } else if (message.startsWith("TemperatureOpen")) {
    controller.setTemperatureOpen(sub.toInt());
  } else if (message.startsWith("HumidityClose")) {
    controller.setHumidityClose(sub.toInt());
  } else if (message.startsWith("HumidityOpen")) {
    controller.setHumidityOpen(sub.toInt());
  } else if (message.startsWith("Mode")) {
    controller.setMode(sub.toInt());
    MODE_STATE = Mode(sub.toInt());  // Update the mode state
  } else if (message.startsWith("Period")) {
    controller.setPeriod(sub.toInt());
  }
}

// Handle getting values
void handleGet(String message) {
  if (message.startsWith("AirTemperature")) {
    Serial.print(controller.information.getAirTemperature());
  } else if (message.startsWith("Settings")) {
    controller.getSettings();
  } else if (message.startsWith("AirHumidity")) {
    Serial.print(controller.information.getAirHumidity());
  } else if (message.startsWith("SoilHumidity")) {
    Serial.print(controller.information.getSoilHumidity());
  } else if (message.startsWith("DoorState")) {
    Serial.print(String(controller.door.getDoorState()));
  }
}

// Handle the rest of the commands
void handleCommands(String message) {
  if (message.startsWith("open")) {
    controller.door.open();
  } else if (message.startsWith("close")) {
    controller.door.close();
  }
}

void handleInput(String message) {
    if (message.startsWith("set")) {
      handleSet(message.substring(3));
    } else if (message.startsWith("get")) {
      handleGet(message.substring(3));
    } else {
      handleCommands(message);
    }
}

void setup() {
  Serial.begin(ESP_BAUD_RATE);
  Serial.println("Initializing...");
  delay(3000);
}

void loop() {
  // Get messages from ESP
  String message;
  if (Serial.available()) {
    message = Serial.readStringUntil('\n');
  }

  // Check for command to set mode
  if (message.startsWith("setMode ")) {
    int begin = message.indexOf(" ");
    int end = message.indexOf("\n");
    if (begin != -1 || end != -1) {
      controller.setMode(message.substring(begin + 1, end - 1).toInt());
      MODE_STATE = Mode(message.substring(begin + 1, end - 1).toInt());
    }
  }

  // Handle input from esp
  handleInput(message);

  // If in automatic mode, check door status
  if (MODE_STATE == AUTOMATIC) {
    controller.checkDoor();
  }
}
