#ifndef INFORMATION_H
#define INFORMATION_H

#include <Adafruit_Sensor.h>
#include <Arduino.h>
#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN 5
#define DHTTYPE DHT11

#define SOIL_PIN A0
#define SOIL_MAX 800
#define SOIL_MIN 400

class Information {
public:
  Information();

  // Send information to esp
  void getInformation();

  float getAirTemperature();

  float getAirHumidity();

  int getSoilHumidity();

private:
  sensor_t sensor;
  DHT_Unified dht = DHT_Unified(DHTPIN, DHTTYPE);
  uint32_t delayMS;
  sensors_event_t event;
};

#endif
