#include "Information.h"

Information::Information() {
  // Initialize DHT sensor
  dht.begin();
  
  // Get sensor information
  dht.temperature().getSensor(&sensor);
  dht.humidity().getSensor(&sensor);
  
  // Calculate delay based on sensor minimum delay
  delayMS = sensor.min_delay / 1000;
}


void Information::getInformation() {
  String humidity = "airHumidity " + String(getAirHumidity()) + "\n";
  String temperature = "airTemperature " + String(getAirTemperature()) + "\n";
  String soil = "soilHumidity " + String(getSoilHumidity()) + "\n";

  Serial.print(humidity + temperature + soil);
}

float Information::getAirTemperature() {
  for (int i = 0; i < 3; i++) {
    dht.temperature().getEvent(&event);
    if (!isnan(event.temperature)) {
      return event.temperature;
    }
    delay(delayMS);
    dht.temperature().getEvent(&event);
  }
  return 0; // Return a default value if sensor not working
}

float Information::getAirHumidity() {
  dht.humidity().getEvent(&event);
  for (int i = 0; i < 3; i++) {
    if (!isnan(event.relative_humidity)) {
      return event.relative_humidity;
    }
    delay(delayMS);
    dht.humidity().getEvent(&event);
  }
  return 0; // Return a default value if sensor not working
}

int Information::getSoilHumidity() {
  int measuredValue = analogRead(SOIL_PIN);
  
  // Map the value to a percentage range
  return map(measuredValue, SOIL_MAX, SOIL_MIN, 0, 100);
}
