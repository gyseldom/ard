# Zpráva
Semestrální práce se zabývala implementací a sestavením prototypu Správce skleníků, který je možné ovládat na dálku pomocí webové stránky. Podmínkou je připojení na lokální WiFi, kterou poskytuje ESP8266. Nabízí manuální ovládání i nastavení hodnot pro automatické ovládání dveří.


## Co se povedlo
Hlavní část programu jsem úspěšně implementoval a Správce skleníku by tak mohl být bez větších úprav a problémů využit v reálném prostředí.

## Co se nepovedlo
V původním návrhu byla i možnost nastavit periodu zavírání dveří. Nastavení této periody je implementováno, ale nemá žádný efekt, protože jsem již nesthil tuto funkcionalitu naimplementovat.

Také v původním návrhu byla funkcionalita notifikování uživatele v zadaných intervalech, ale v zhledem použití ESP8266 na místo původně zamýšleného SMS800l se to stalo logisticky neproviditelným, tak jsem tuto funkcionalitu vynechal.

User experience by mohla být o něco lepší. Momentálně dochází k delším problevám při načítání stránky v důsledku provádění měření.


## Problémy
Největšími problémy byly dozajista zprostředkování komunikace mezi arduinem a ESP8266. Nejdříve kvůli špatnému zapojení RX a TX pinů a poté špatně zvolenou implementací pomocí SoftwareSerial knihovny, která často posílala špatná data. To jsem opravil využím UART pinů a HardwareSerial, které je mnohem spolehlivější.

Překážkou mi také byl nedostatek materiálů. To zejména těch mechanických. Otočný naviják pomocí kterého jsou otevírány dveře je vyrobený z kartonu a lepící pásky. A na místo provázku či tyče jsem byl nucen použít šicí niť.
