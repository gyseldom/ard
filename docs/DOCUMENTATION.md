# Teoretická příprava


## Vlastnosti projektu

Správce skleníku bude nastavitelný a ovladatelný přes WIFI. Uživatel bude mít k dispozici následující příkazy k ovládání:

`open`
Otevře dveře skleníku.

`close`
Zavře dveře skleníku.

`getInformation`
Získá informace o teplotě, vlhkosti půdy a vzduchu.

`setTemperatureClose`
Nastaví hodnotu teploty při které se automaticky zavřou dveře.

`setTemperatureOpen`
Nastaví hodnotu teploty při které se automaticky otevřou dvěře.

`setHumidityClose`
Nastaví hodnotu vlhkosti při které se automaticky zavřou dveře.

`setHumidityOpen`
Nastaví hodnotu vlhkosti při které se automaticky otevřou dvěře.

`setMode`
Nastaví mode operace.
Managed mode - skleník se zavírá pouze na signály od uživatele.
Automatic mode - skleník se zavírá pouze podle nastavené teploty.

`setPeriod`
Nastaví velikost periody mezi automatickým zasláním informací.
Interval má hodnotu v hodinách.
0 -> posílání vypnuto.

`getSettings`
Získá právě nastavené hodnoty ovládání. Mód, HumidityClose, HumidityOpen, TemperatureClose, TemperatureOpen, Period a zda jsou dveře otevřené či zavřené.

## Datové struktury

Ovládá dveře. Uchovává si svůj stav, takže pokud jsou již dveře zavřené, tak je znovu nezavírá.
```c++
Door {

open();

close();
}
```

Je zodpovědná za získávání informací ohledně teploty a vlhkosti.
```c++
Information {

getInformation();
}
```

Je zodpovědná za ovládání ostatních prvků a uchovává nastavení poskytnuté uživatelem.
```c++
Controller {

setTemperatureOpen();

setTemperatureClose();

setHumidityOpen();

setHumidityClose();

setMode();

setPeriod();

getSettings();

}
```


## Stavový automat

### Mód

![Mode state](./mode-state-machine.png)


### Dveře

Následující stavový automat se použije v případě, že je mód nastaven na Automatický.

![Door state](./door-state-machine.png)

## Elektrické schéma

![Schema](./schema.pdf)


# Hardwarová dokumentace

## Potřebné součástky

  - Arduino
  - ESP8266
  - Krokový motor s řadičem
  - DHT11 / DHT22
  - Kapacitní měřič vlhkosti půdy
  - Převodník napětí 5V => 3.3V
  - Male to male propojovací kabely
  - Male to female propojovací kabely


K nahrání programu na arduino a ESP8266 je nevhodnější použít Arduino IDE. Je potřeba si nejdříve stáhnout board manager pro desku ESP8266 a následující knihovny.

```
<Adafruit_Sensor.h>
<Arduino.h>
<DHT.h>
<DHT_U.h>
<ESP8266WiFi.h>
<ESP8266WebServer.h>
```

## Zapojení


### Schema zapojeni pri instalaci ESP

Program pro ESP je nutno nainstalovat samostatně a mít správně zapojené jak arduino tak ESP8266.

Je potřeba zapojit ESP8266 k arduinu v módu pro nahrání programu na ESP8266. Toho lze dosáhnout běžným zapojením ESP a dodatečně spojit GPIO-0 s GROUND a RESET od arduina s ground. Pozor, ten je potřeba připojit až když je arduino připojeno k napájení, jinak se program nenahraje.

Důležité je také zapojení RX ESP8266 na RX arduina a TX ESP8266 na TX arduina.


![Esp](./esp.png)


### Schéma zapojení při instalaci Arduina

Pro nainstalování arduina je potřeba odpojit ESP8266 z VCC a GROUND od arduina, jinak se nahrání programu nemusí podařit.

![Arduino](./arduino.png)

### Schéma zapojení při provozu

Pro provoz je třeba zapojit arduino i esp8266 podle následujícího schématu.

Krokový motor připojíme pomocí řadiče ke 4 digitálním pinům, VCC a GROUND od arduina. Volitelně lze motor napájet z externího zdroje.

DHT11 / DHT22 zapojime na VCC a GROUND arduina a jeden digitální pin.

Kapacitní meřič vlhkosti půdy připojíme na VCC a GROUND arduina a jeden analogový pin.

Důležité je RX esp na TX arduina a TX esp na RX arduina. Také je potřeba provést redukci z 5V na 3.3V jinak by mohlo dojít k poškození ESP desky.

![Provoz](./provoz.png)


# Uživatelský manuál 


Po úspěšném nahrání programu na esp a arduino a správném zapojení již můžeme Správce skleníku používat.

K použití se stačí připojit na WiFi síť kterou ESP poskytuje. V základním nastavení je SSID `sklenik` a heslo je `123456789`.

Po připojení k síti je potřeba se připojit na webovou stránku s IP adresou `192.168.1.10`. Podporováno je pouze `http` připojení.

Po načtení stránky se již může skleník ovládat.

Správce má 2 operační módy: 

  - Automatický - při kterém si zavírání a otevírání dveří hlídá sám pomocí nastavených hodnot.
  - Manuální - při kterém dveře ovládá pouze uživatel.

Na hlavní stránce správce skleníku zobrazuje naměřené údaje. Ty se aktualizují při každém obnovení stránky.

Údaj o vlhkosti je vyjádřen v procentech relativně k nejnižší naměřené hodnotě v suchém strahovském pokoji na vzduchu. Nejvyšší možná hodnota je pak nejvyšší hodnota senzoru.
