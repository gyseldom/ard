# Správce skleníku



## Specifikace zadání

Správce skleníku bude dálkově ovladatelný poloautomatický ovladač dveří skleníku, který bude periodicky informovat uživatele o těplotě vzduchu, vlhkosti vzduchu a vlhkosti půdy. Bude moct otevřít a zavřít dveře od skleníku na základě teploty a vlhkosti vzduchu, které nastaví uživatel. Ovladatelné rozhraní bude pomocí SMS nebo WIFI. Uživatel bude moct poslat signál správci na vynucené zavření případně otevření dveří a také signál pro získání hodnot teploty a vlhkosti.
